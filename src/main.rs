#![feature(proc_macro_hygiene)]
#![windows_subsystem = "windows"]

extern crate winapi;
extern crate winrt;
extern crate wchar;

use std::mem;
use wchar::wch_c;
use winrt::*;
use winrt::windows::ui::notifications::*;

use winapi::shared::minwindef::{DWORD, LPARAM, BOOL, TRUE, FALSE, UINT};
use winapi::shared::ntdef::{LONG, LPCWSTR};
use winapi::shared::windef::HWND;

#[link(name="shell32")]
extern "system" {
    fn StrStrNW(text: LPCWSTR, search: LPCWSTR, cchMax: UINT) -> LPCWSTR;
}

fn find_skype_window() -> Option<HWND> {
    use winapi::um::winuser::{ EnumWindows, GetClassNameW, GetWindowTextW };

    struct CallbackParam {
        pub skype_wnd: Option<HWND>,
        pub text_buf: Vec<u16>,
    };

    unsafe extern "system" fn wnd_enum_proc(h_wnd: HWND, lp: LPARAM) -> BOOL {
        use winapi::um::winbase::lstrcmpW;

        const WINRT_WINDOW_CLASS: &[u16] = wch_c!("ApplicationFrameWindow");
        const SKYPE_WINDOW_TITLE_PART: &[u16] = wch_c!("Skype");

        let param: &mut CallbackParam = mem::transmute(lp);
        let len = GetClassNameW(h_wnd,
                      param.text_buf.as_mut_ptr(),
                      param.text_buf.len() as LONG);

        if len == 0 {
            return TRUE;
        }

        if lstrcmpW(WINRT_WINDOW_CLASS.as_ptr(), param.text_buf.as_ptr()) != 0 {
            return TRUE;
        }

        let len = GetWindowTextW(h_wnd,
                                 param.text_buf.as_mut_ptr(),
                                 param.text_buf.len() as LONG);

        if len == 0 {
            return TRUE;
        }

        if StrStrNW(param.text_buf.as_ptr(),
                    SKYPE_WINDOW_TITLE_PART.as_ptr(),
                    len as UINT).is_null() {
            return TRUE;
        }

        // Found!
        param.skype_wnd = Some(h_wnd);
        FALSE
    }

    let mut callback_param = CallbackParam {
        skype_wnd: None,
        text_buf: vec![0u16; 1024],
    };

    unsafe {
        EnumWindows(Some(wnd_enum_proc), mem::transmute(&mut callback_param));
    }

    callback_param.skype_wnd
}


fn highlight_skype_window() {
    use winapi::um::winuser::{ FLASHWINFO,
                               FLASHW_TIMERNOFG,
                               FLASHW_TRAY,
                               FlashWindowEx };

    if let Some(skype_hwnd) = find_skype_window() {
        let mut flash_info = FLASHWINFO {
            cbSize: mem::size_of::<FLASHWINFO>() as DWORD,
            hwnd: skype_hwnd,
            dwFlags: FLASHW_TIMERNOFG | FLASHW_TRAY,
            uCount: 0,
            dwTimeout: 0,
        };
        unsafe {
            FlashWindowEx(&mut flash_info);
        }
    }
}


fn main() {
    let _ctx = RuntimeContext::init(); // initialize the Windows Runtime
    let listener = management::UserNotificationListener::get_current()
        .expect("Failed to get UNL")
        .expect("UNL is empty");

    // Seems like installing notification change listener requires
    // some UWP infrastructure to be running. Will have to do this
    // in a most simple manner for the moment

    let mut is_notifying = false;

    loop {
        std::thread::sleep(std::time::Duration::from_secs(1));

        let notification_count = {
            let mut notification_count = 0;

            let notifications = if let Ok(notifications) = listener
                .get_notifications_async(NotificationKinds::Toast) {
                notifications
            } else {
                continue;
            };

            let notifications = if let Ok(Some(notifications)) = notifications.blocking_get() {
                notifications
            } else {
                continue;
            };

            let size = if let Ok(size) = notifications.get_size() {
                size
            } else {
                continue;
            };

            for i in 0u32 .. size {
                let notification = if let Ok(Some(notification)) = notifications.get_at(i) {
                    notification
                } else {
                    continue;
                };

                let app_info = if let Ok(Some(app_info)) = notification.get_app_info() {
                    app_info
                } else {
                    continue;
                };

                let app_id = if let Ok(app_id) = app_info.get_app_user_model_id() {
                    app_id.to_string()
                } else {
                    continue;
                };

                // Skype has IDs like Microsoft.SkypeApp_kzf8qxf38zg5c!App
                if app_id.starts_with("Microsoft.SkypeApp") {
                    notification_count += 1;
                }
            }

            notification_count
        };

        if notification_count > 0 {
            if !is_notifying {
                highlight_skype_window();
                is_notifying = true;
            }
        } else {
            is_notifying = false;
        }

    }
}